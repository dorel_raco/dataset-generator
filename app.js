/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */

// Import the Google Cloud client library using default credentials
const {BigQuery} = require('@google-cloud/bigquery');
const bigquery = new BigQuery();
const {TABLES} = require('./tables')


exports.app = async (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');
    const datasets = ['d1','d2','d3','d4','d5','d6','d7','d8','d9','d10','d11','d12','d13','d14','d15','d16','d17','d18','d19','d20']
    for(const dataset of datasets){
        await createDataset(dataset);
        console.log(`dataset ${dataset} created.`);
    }
    res.status(200).json("all datasets created. done.")
    // if (req.method === 'OPTIONS') {
    //     // Send response to OPTIONS requests
    //     res.set('Access-Control-Allow-Methods', 'GET');
    //     res.set('Access-Control-Allow-Headers', 'Content-Type');
    //     res.set('Access-Control-Max-Age', '3600');
    //     res.status(204).send('');
    // } else {
    //     res.status(200).json(await query(req.query));
    // }
};

const createDataset = async (datasetId)=> {
    const options = {
        location: 'europe-west1',
    };

    // Create a new dataset
    const [dataset] = await bigquery.createDataset(datasetId, options);
    console.log(`Dataset ${dataset.id} created.`);

    await createTablesForDataset(datasetId);


}

const createTablesForDataset= async (datasetId)=>{
    console.log(TABLES);
    for (const t of TABLES) {
        console.log(t);
        const tableOptions = {
            schema: t.schema,
            location: 'europe-west1',
        };
        // Create a new table in the dataset
        const [table] = await bigquery
            .dataset(datasetId)
            .createTable(t.id, tableOptions);

        console.log(`Table ${table.id} created.`);
    }
}